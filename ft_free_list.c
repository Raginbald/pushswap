/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_list.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 16:34:25 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 14:22:41 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "struct.h"

void	ft_free_list(t_nod *head, int *len)
{
	t_nod	*current;
	t_nod	*next;
	int		i;
	int		stop;

	i = 0;
	stop = *len;
	current = head;
	while (i < stop)
	{
		next = current->nxt;
		free(current);
		current = next;
		++i;
	}
	head = NULL;
}
