/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate_r.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 14:31:42 by graybaud          #+#    #+#             */
/*   Updated: 2015/06/09 10:56:44 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_ra(t_env *env)
{
	if (env->a->len > 1)
	{
		ft_swap_nod(env->a->head, env->a->head->nxt);
		ft_display_action(env, "ra ", env->flag);
	}
}

void	ft_rb(t_env *env)
{
	if (env->b->len > 1)
	{
		ft_swap_nod(env->b->head, env->b->head->nxt);
		ft_display_action(env, "rb ", env->flag);
	}
}

void	ft_rr(t_env *env)
{
	if (env->a->len > 1 && env->b->len > 1)
	{
		ft_swap_nod(env->a->head, env->a->head->nxt);
		ft_swap_nod(env->b->head, env->b->head->nxt);
		ft_display_action(env, "rr ", env->flag);
	}
}
