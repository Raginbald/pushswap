/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_option.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 14:39:46 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 14:27:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int	ft_init_option(t_env *env, const char *opt)
{
	if (ft_isdigit_str(opt))
		return (0);
	else
	{
		while (*opt)
		{
			if (ft_iswhitespace(*opt))
				++opt;
			if (*opt && *opt == 'v')
				env->flag |= F_VERBOSE;
			if (*opt && *opt == 'c')
				env->flag |= F_COLOR;
			if (*opt && *opt == 'l')
				env->flag |= F_LAST;
			++opt;
		}
	}
	if (env->flag > 0)
		return (1);
	return (0);
}
