/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sorted.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:44:30 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:36:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		ft_is_sorted(t_env *env)
{
	t_nod	*it;
	t_nod	*save;
	t_nod	*stop;

	stop = LA_PRV;
	save = LA_NXT;
	it = MIN_A;
	env->flag |= F_MUTE;
	while (LA_NXT != MIN_A)
		ft_ra(env);
	stop = LA_PRV;
	while (LA_NXT != stop && LA_NB1 < LA_NB2)
	{
		LA_NXT->color = 1;
		ft_ra(env);
	}
	it = LA_NXT;
	SWAP = LA_NXT;
	while (LA_NXT != save)
		ft_ra(env);
	env->flag &= ~F_MUTE;
	if (it == stop)
		return (1);
	return (0);
}
