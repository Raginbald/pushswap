/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/02 16:39:50 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 14:34:35 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	ft_display_name(const char *c)
{
	ft_putchar_fd((*c), 1);
	ft_putstr_fd((": "), 1);
}

static void	ft_putnbr_char(int flag, int *color, const int *nb, const char c)
{
	if (flag & F_COLOR)
	{
		if (*color == 1)
			ft_putstr_fd("\x1b[32;01m", 1);
		else
			ft_putstr_fd("\x1b[31;01m", 1);
		ft_putnbr(*nb);
		ft_putstr_fd("\x1b[0m", 1);
		ft_putchar_fd((c), 1);
	}
	else
	{
		ft_putnbr(*nb);
		ft_putchar_fd((c), 1);
	}
}

static void	ft_display(int flag, const t_root *root)
{
	t_nod	*tmp;
	int		i;

	i = 0;
	tmp = root->head->nxt;
	ft_display_name(&root->n);
	while (i < root->len - 1)
	{
		ft_putnbr_char(flag, &tmp->color, &tmp->nb, ' ');
		tmp = tmp->nxt;
		++i;
	}
	ft_putnbr_char(flag, &tmp->color, &tmp->nb, '\n');
}

void		ft_print_list(const t_env *env)
{
	if (LA_LEN > 0)
		ft_display(env->flag, env->a);
	if (LB_LEN > 0)
		ft_display(env->flag, env->b);
}
