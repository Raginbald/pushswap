/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_last_action.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:44:41 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 11:44:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_last_action(t_env *env, void (*ft_move)(t_env *), const char *s)
{
	env->flag |= F_MUTE;
	ft_move(env);
	env->flag &= ~F_MUTE;
	ft_display_action(env, s, env->flag);
	if (env->flag & F_LAST)
	{
		ft_putchar_fd('\n', 1);
		ft_print_list(env);
	}
}
