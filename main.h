/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 17:04:44 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:47:52 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include "struct.h"
# define SORTED "\n"
# define ERROR "Error"
# define BAD_ALLOC "malloc() fail : exit(failure)"
# define NO_ARG "usage: ./ft_pushswap [-lvc] 1 2 3 ... x"
# define BAD_ARG "ft_init_list() fail : bad argument, not Int"

# define F_VERBOSE	1
# define F_COLOR	2
# define F_LAST		4
# define F_SORTED	8
# define F_MUTE		16
# define MIN_A 		env->min_a
# define MIN_B 		env->min_b
# define MAX_A 		env->max_a
# define SWAP 		env->swap
# define LA_LEN 	env->a->len
# define LB_LEN 	env->b->len
# define LA_NXT		env->a->head->nxt
# define LB_NXT		env->b->head->nxt
# define LA_PRV		env->a->head->prv
# define LA_NB1		env->a->head->nxt->nb
# define LA_NB2		env->a->head->nxt->nxt->nb
# define LA_COLOR	env->a->head->nxt->color

t_env	*ft_init_env(void);
t_root	*ft_init_head(t_env *env, const char c);
void	ft_init_list(t_env *env, char **av);
t_nod	*ft_init_nod(t_env *env, t_nod *nxt, t_nod *prv, int id);
int		ft_init_option(t_env *env, const char *opt);
t_nod	*ft_init_tab(t_env *env, int *flag, int len);
void	ft_exit(t_env *env, const char *str, int flag);
void	ft_free_list(t_nod *head, int *len);
void	ft_display_action(const t_env *env, const char *act, const char flag);
void	ft_print_list(const t_env *env);
void	ft_push_back(t_env *env, t_nod *head);
void	ft_merge_sort(t_env *env, t_nod *tab, int size);
int		ft_is_sorted(t_env *env);
void	ft_find_min_max(t_env *env);
void	ft_little_sort(t_env *env);
void	ft_find_double(t_env *env);
void	ft_find_disorder(t_env *env);
void	ft_last_action(t_env *env, void (*t_move)(t_env *), const char *s);
void	ft_move_to_nod_push_max(t_env *env, t_nod *n);
void	ft_move_to_nod(t_env *env, t_nod *n);
void	ft_sort_small_disorder(t_env *env);
void	ft_sort_min_max(t_env *env);
void	ft_last_action(t_env *env, void (*t_move)(t_env *), const char *s);
void	ft_sa(t_env *env);
void	ft_sb(t_env *env);
void	ft_ss(t_env *env);
void	ft_push_to_list(t_root *l1, t_root *l2);
void	ft_pa(t_env *env);
void	ft_pb(t_env *env);
void	ft_swap_nod(t_nod *n1, t_nod *n2);
void	ft_ra(t_env *env);
void	ft_rb(t_env *env);
void	ft_rr(t_env *env);
void	ft_rra(t_env *env);
void	ft_rrb(t_env *env);
void	ft_rrr(t_env *env);
int		ft_strlen(const char *s);
void	ft_putchar_fd(const char c, const int fd);
void	ft_putstr_fd(char const *s, const int fd);
void	ft_putnbr(int n);
int		ft_abs(int n);
int		ft_isdigit(const int c);
int		ft_isdigit_str(const char *str);
int		ft_iswhitespace(const int c);
int		ft_isdigit_str(const char *str);
int		ft_atoi(t_env *env, const char *str);
void	ft_swap_int(int *a, int *b);

#endif
