/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_merge_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/15 10:09:34 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:39:34 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "main.h"

static void	ft_init_it(t_env *env, t_merge *it, int size, int mid)
{
	it->i = 0;
	it->j = mid;
	it->k = 0;
	if (!(it->tmp = (t_nod *)malloc(sizeof(t_nod) * size)))
		ft_exit(env, BAD_ALLOC, 1);
}

static void	ft_merge(t_env *env, t_nod *tab, int size, int mid)
{
	t_merge	it;

	ft_init_it(env, &it, size, mid);
	while (it.k < size)
	{
		if (it.j == size)
			it.tmp[it.k].nb = tab[it.i++].nb;
		else if (it.i == mid)
			it.tmp[it.k].nb = tab[it.j++].nb;
		else if (tab[it.j].nb < tab[it.i].nb)
			it.tmp[it.k].nb = tab[it.j++].nb;
		else
			it.tmp[it.k].nb = tab[it.i++].nb;
		it.k++;
	}
	it.i = 0;
	while (it.i < size)
		tab[it.i++].nb = it.tmp[it.i].nb;
	free(it.tmp);
}

void		ft_merge_sort(t_env *env, t_nod *tab, int size)
{
	int mid;

	mid = size / 2;
	if (size < 2)
		return ;
	ft_merge_sort(env, tab, mid);
	ft_merge_sort(env, tab + mid, size - mid);
	ft_merge(env, tab, size, mid);
}
