/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_disorder.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/10 11:10:15 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:33:10 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		ft_find_disorder(t_env *env)
{
	t_nod	*stop;
	t_nod	*i;

	i = LA_NXT;
	stop = LA_PRV;
	while (i != stop)
	{
		if (i->nb > i->nxt->nb)
		{
			++(env->disorder);
			MIN_B = i;
		}
		if (i->nb < i->nxt->nb)
			i->color = 1;
		i = i->nxt;
	}
}
