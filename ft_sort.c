/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/08 15:26:49 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:41:51 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	ft_sort_little_la(t_env *env)
{
	ft_find_min_max(env);
	if (LA_NXT == MIN_A && LA_NXT->nxt == MAX_A)
	{
		ft_sa(env);
		ft_ra(env);
	}
	else if (LA_NXT == MAX_A && LA_PRV == MIN_A)
	{
		ft_sa(env);
		ft_rra(env);
	}
	else if (LA_NXT->nxt == MIN_A && LA_PRV == MAX_A)
		ft_sa(env);
	else if (LA_NXT->nxt == MAX_A && LA_PRV == MIN_A)
		ft_rra(env);
	else if (LA_NXT == MAX_A && LA_NXT->nxt == MIN_A)
		ft_ra(env);
}

static void	ft_sort_dependancy_1(t_env *env)
{
	ft_sb(env);
	ft_rb(env);
	env->tmp = 0;
}

static void	ft_sort_dependancy_2(t_env *env)
{
	ft_move_to_nod(env, MAX_A);
	ft_pb(env);
	ft_rb(env);
}

void		ft_sort_min_max(t_env *env)
{
	env->tmp = 0;
	while (LA_LEN > 3)
	{
		ft_find_min_max(env);
		ft_move_to_nod_push_max(env, MIN_A);
		ft_pb(env);
		if (LA_LEN == 3)
			break ;
		if (env->tmp)
			ft_sort_dependancy_1(env);
		else
			ft_sort_dependancy_2(env);
	}
	ft_sort_little_la(env);
	while (LB_LEN > 0)
		ft_pa(env);
	ft_find_min_max(env);
	ft_is_sorted(env);
	while (LA_NXT->nxt != MIN_A)
		ft_ra(env);
	ft_last_action(env, ft_ra, "ra\n");
}
