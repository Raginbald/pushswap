/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reverse_rotate_rr.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 14:32:00 by graybaud          #+#    #+#             */
/*   Updated: 2015/02/17 14:32:02 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_rra(t_env *env)
{
	if (env->a->len > 1)
	{
		ft_swap_nod(env->a->head->prv, env->a->head);
		ft_display_action(env, "rra ", env->flag);
	}
}

void	ft_rrb(t_env *env)
{
	if (env->b->len > 1)
	{
		ft_swap_nod(env->b->head->prv, env->b->head);
		ft_display_action(env, "rrb ", env->flag);
	}
}

void	ft_rrr(t_env *env)
{
	if (env->a->len > 1 && env->b->len > 1)
	{
		ft_swap_nod(env->a->head->prv, env->a->head);
		ft_swap_nod(env->b->head->prv, env->b->head);
		ft_display_action(env, "rrr ", env->flag);
	}
}
