/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_list.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 14:39:57 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 14:27:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_init_list(t_env *env, char **av)
{
	int i;

	i = env->id;
	while (av[i])
	{
		if (ft_isdigit_str(av[i]))
		{
			env->tmp = ft_atoi(env, av[i]);
			ft_push_back(env, env->a->head);
		}
		else
			ft_exit(env, ERROR, 0);
		++i;
	}
}
