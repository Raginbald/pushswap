/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_action.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 17:19:14 by graybaud          #+#    #+#             */
/*   Updated: 2015/06/09 18:08:28 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_display_action(const t_env *env, const char *act, const char flag)
{
	if (flag & F_MUTE)
		return ;
	if (!(flag & F_COLOR))
		ft_putstr_fd((act), 1);
	else
	{
		ft_putstr_fd("\x1b[36;01m", 1);
		ft_putstr_fd((act), 1);
		ft_putstr_fd("\x1b[0m", 1);
	}
	if (flag & F_VERBOSE && !(flag & F_LAST))
	{
		ft_putchar_fd('\n', 1);
		ft_print_list(env);
	}
}
