# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 13:48:28 by graybaud          #+#    #+#              #
#    Updated: 2015/06/09 10:30:18 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= push_swap
DEBUG		= debug_$(NAME)
CC			= cc
CFLAGS		= -Wall -Wextra -Werror
INCLUDES	= -I./
DFLAGS		= -g
RM			= rm -Rf
H			= 	main.h \
				struct.h

SRC			=	ft_display_action.c \
				ft_exit.c 			\
				ft_find_disorder.c 	\
				ft_find_double.c 	\
				ft_find_min_max.c 	\
				ft_free_list.c 		\
				ft_init_env.c 		\
				ft_init_head.c 		\
				ft_init_list.c 		\
				ft_init_nod.c 		\
				ft_init_option.c 	\
				ft_init_tab.c 		\
				ft_is_sorted.c 		\
				ft_last_action.c 	\
				ft_little_sort.c 	\
				ft_move_nod.c 		\
				ft_merge_sort.c 	\
				ft_print_list.c 	\
				ft_push_back.c 		\
				ft_push_p.c 		\
				ft_push_to_list.c 	\
				ft_reverse_rotate_rr.c \
				ft_rotate_r.c		\
				ft_sort_small_disorder.c \
				ft_sort.c 			\
				ft_swap_nod.c 		\
				ft_swap_s.c 		\
				libft_a.c 			\
				libft_b.c 			\
				main.c

OBJ			= $(SRC:.c=.o)

$(NAME): $(OBJ) $(H)
	@echo "building $(NAME) ... "
	@$(CC) $(CFLAGS) $(INCLUDES) -o $@ $(OBJ)
	@echo "$(NAME) created !"

%.o: %.c
	@$(CC) $(CFLAGS) $(INCLUDES) -c $^ -o $@

all: $(NAME)

test: $(OBJ) $(H)
	@echo "building $(DEBUG) ... "
	@$(CC) $(CFLAGS) $(INCLUDES) $(DFLAGS) $(SRC) -o $(DEBUG)
	@echo "$(DEBUG) created !"

clean:
	@$(RM) $(DEBUG) $(DEBUG).dSYM $(OBJ)
	@echo "clean done !"

fclean:clean
	@$(RM) $(NAME)
	@echo "fclean done !"

re: fclean all

.PHONY:all clean fclean re
