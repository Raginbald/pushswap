/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_small_disorder.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 10:45:39 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:39:58 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_sort_small_disorder(t_env *env)
{
	int a;

	a = ft_is_sorted(env);
	while (env->disorder && !a)
	{
		ft_move_to_nod(env, MIN_B);
		ft_sa(env);
		env->disorder--;
		a = ft_is_sorted(env);
	}
	ft_move_to_nod(env, MIN_A);
}
