/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/15 11:07:39 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:35:40 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "main.h"

t_nod	*ft_init_tab(t_env *env, int *flag, int len)
{
	t_nod	*tab;
	int		i;

	i = 0;
	if (!(tab = (t_nod *)malloc(sizeof(t_nod) * len)))
		ft_exit(env, BAD_ALLOC, 1);
	*flag |= F_MUTE;
	while (i < len)
	{
		tab[i].nb = LA_NB1;
		tab[i].color = LA_COLOR;
		++i;
		ft_ra(env);
	}
	*flag &= ~F_MUTE;
	return (tab);
}
