/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 12:02:39 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 14:30:03 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_sa(t_env *env)
{
	if (env->a->len > 1)
	{
		ft_swap_nod(LA_NXT, LA_NXT->nxt);
		ft_display_action(env, "sa ", env->flag);
	}
}

void	ft_sb(t_env *env)
{
	if (env->b->len > 1)
	{
		ft_swap_nod(LB_NXT, LB_NXT->nxt);
		ft_display_action(env, "sb ", env->flag);
	}
}

void	ft_ss(t_env *env)
{
	if (env->a->len > 1 && env->b->len > 1)
	{
		ft_swap_nod(LA_NXT, LA_NXT->nxt);
		ft_swap_nod(LB_NXT, LB_NXT->nxt);
		ft_display_action(env, "ss ", env->flag);
	}
}
