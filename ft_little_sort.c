/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_little_sort.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/08 16:46:36 by graybaud          #+#    #+#             */
/*   Updated: 2015/10/20 11:03:12 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_little_sort(t_env *env)
{
	if (!ft_is_sorted(env))
	{
		if (LA_NB2 != MIN_A->nb)
			ft_sa(env);
		else
			ft_last_action(env, ft_sa, "sa\n");
	}
	if (MIN_A == LA_NXT)
		return ;
	else if (LA_NXT->nxt == MIN_A)
		ft_last_action(env, ft_ra, "ra\n");
	else
		ft_last_action(env, ft_rra, "rra\n");
}
