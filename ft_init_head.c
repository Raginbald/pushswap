/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_head.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 14:40:28 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/29 14:40:29 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <limits.h>
#include "main.h"

t_root	*ft_init_head(t_env *env, const char c)
{
	t_root	*tmp;

	if (!(tmp = (t_root *)malloc(sizeof(t_root))))
		ft_exit(env, BAD_ALLOC, 1);
	tmp->len = 0;
	tmp->n = c;
	tmp->head = ft_init_nod(env, NULL, NULL, -1);
	tmp->head->nxt = tmp->head;
	tmp->head->prv = tmp->head;
	tmp->head->id = -1;
	tmp->head->nb = INT_MAX;
	tmp->head->color = -1;
	return (tmp);
}
