/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_p.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/02 16:34:44 by graybaud          #+#    #+#             */
/*   Updated: 2015/10/02 16:34:46 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		ft_pa(t_env *env)
{
	if (LB_LEN > 0)
	{
		ft_push_to_list(env->b, env->a);
		ft_display_action(env, "pa ", env->flag);
	}
}

void		ft_pb(t_env *env)
{
	if (LA_LEN > 0)
	{
		ft_push_to_list(env->a, env->b);
		ft_display_action(env, "pb ", env->flag);
	}
}
