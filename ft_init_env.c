/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 14:40:36 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/29 14:40:37 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "main.h"

t_env	*ft_init_env(void)
{
	t_env	*tmp;

	if (!(tmp = (t_env *)malloc(sizeof(t_env))))
		ft_exit(tmp, BAD_ALLOC, 1);
	tmp->max_a = NULL;
	tmp->min_a = NULL;
	tmp->swap = NULL;
	tmp->tab = NULL;
	tmp->disorder = 0;
	tmp->stop = NULL;
	tmp->flag = 0;
	tmp->tmp = 0;
	tmp->id = 0;
	tmp->a = ft_init_head(tmp, 'a');
	tmp->b = ft_init_head(tmp, 'b');
	return (tmp);
}
