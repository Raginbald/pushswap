/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 15:50:37 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:43:03 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "main.h"

static void	ft_sort(t_env *env)
{
	if (LA_LEN == 1)
		ft_exit(env, SORTED, 0);
	else if (LA_LEN == 2 && LA_NB1 > LA_NB2)
		ft_last_action(env, ft_ra, "ra");
	else if (LA_LEN == 3)
		ft_little_sort(env);
	else if (env->tmp == 1)
		ft_move_to_nod(env, MIN_A);
	else if (env->disorder == 1)
		ft_sort_small_disorder(env);
	else
		ft_sort_min_max(env);
}

int			main(int ac, char **av)
{
	t_env	*env;

	env = NULL;
	if (ac != 1)
	{
		env = ft_init_env();
		env->id = ft_init_option(env, av[1]);
		ft_init_list(env, &av[1]);
		env->tab = ft_init_tab(env, &env->flag, LA_LEN);
		ft_merge_sort(env, env->tab, LA_LEN);
		ft_find_double(env);
		ft_find_disorder(env);
		ft_find_min_max(env);
		env->tmp = ft_is_sorted(env);
		ft_sort(env);
	}
	else
		ft_exit(env, ERROR, 1);
	return (0);
}
