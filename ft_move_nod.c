/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_nod.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:44:00 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:37:54 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	ft_move_to_if(t_env *env, t_nod *stop, void (*t_move)(t_env *))
{
	while (LA_NXT != stop)
	{
		if (LA_NXT == MAX_A && LA_LEN > 4)
		{
			ft_pb(env);
			env->tmp = 1;
			if (LA_NXT == stop)
				break ;
		}
		t_move(env);
	}
}

void		ft_move_to_nod_push_max(t_env *env, t_nod *n)
{
	if (n == LA_NXT)
		return ;
	if (n->id <= LA_LEN / 2)
		ft_move_to_if(env, n, ft_ra);
	else
		ft_move_to_if(env, n, ft_rra);
}

static void	ft_move_to(t_env *env, t_nod *stop, void (*t_move)(t_env *))
{
	while (LA_NXT != stop)
		t_move(env);
}

void		ft_move_to_nod(t_env *env, t_nod *n)
{
	if (n == LA_NXT)
		return ;
	if (n->id <= LA_LEN / 2)
		ft_move_to(env, n, ft_ra);
	else
		ft_move_to(env, n, ft_rra);
}
