/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 16:38:03 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:46:00 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct s_env	t_env;
typedef struct s_nod	t_nod;
typedef struct s_root	t_root;
typedef struct s_merge	t_merge;
typedef void	(*t_move)(t_env *env);

struct	s_env
{
	t_nod	*max_a;
	t_nod	*min_a;
	t_nod	*min_b;
	t_nod	*swap;
	t_nod	*tab;
	int		disorder;
	t_nod	*stop;
	int		flag;
	int		tmp;
	int		id;
	t_root	*a;
	t_root	*b;
};

struct	s_nod
{
	int		id;
	int		nb;
	int		color;
	t_nod	*prv;
	t_nod	*nxt;
};

struct	s_root
{
	int		len;
	char	n;
	t_nod	*head;
};

struct	s_merge
{
	int		i;
	int		j;
	int		k;
	t_nod	*tmp;
};

#endif
