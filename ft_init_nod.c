/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_nod.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 14:40:20 by graybaud          #+#    #+#             */
/*   Updated: 2015/09/29 14:40:21 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "main.h"

t_nod	*ft_init_nod(t_env *env, t_nod *nxt, t_nod *prv, int id)
{
	t_nod *tmp;

	if (!(tmp = (t_nod *)malloc(sizeof(t_nod))))
		ft_exit(env, BAD_ALLOC, 1);
	tmp->id = id;
	tmp->nb = env->tmp;
	tmp->color = 0;
	tmp->nxt = nxt;
	tmp->prv = prv;
	return (tmp);
}
