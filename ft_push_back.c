/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_back.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 16:30:37 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:36:26 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_push_back(t_env *env, t_nod *head)
{
	t_nod *back;

	back = ft_init_nod(env, head, head->prv, env->a->len++);
	head->prv->nxt = back;
	head->prv = back;
}
