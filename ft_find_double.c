/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_double.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/08 16:45:56 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:34:20 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

static void	ft_kill(t_env *env, int nb1, int nb2)
{
	if (nb1 == nb2)
		ft_exit(env, ERROR, 0);
}

void		ft_find_double(t_env *env)
{
	t_nod	*stop;
	t_nod	*i;
	t_nod	*j;

	i = LA_NXT;
	stop = LA_PRV;
	j = i->nxt;
	if (LA_LEN == 1)
		return ;
	if (LA_LEN == 2)
		ft_kill(env, i->nb, j->nb);
	while (i != stop->prv)
	{
		while (j != stop)
		{
			ft_kill(env, i->nb, j->nb);
			j = j->nxt;
		}
		ft_kill(env, i->nb, j->nb);
		i = i->nxt;
		j = i->nxt;
		ft_kill(env, i->nb, j->nb);
	}
}
