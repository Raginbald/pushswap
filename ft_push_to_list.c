/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_to_list.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 14:31:04 by graybaud          #+#    #+#             */
/*   Updated: 2015/06/09 12:02:04 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "struct.h"

void	ft_push_to_list(t_root *l1, t_root *l2)
{
	t_nod *tmp;

	tmp = l1->head->nxt;
	l1->head->nxt = tmp->nxt;
	l1->head->nxt->prv = l1->head;
	tmp->prv = l2->head;
	tmp->nxt = l2->head->nxt;
	l2->head->nxt->prv = tmp;
	l2->head->nxt = tmp;
	l2->head->nxt->id = l2->len++;
	l1->len -= 1;
}
