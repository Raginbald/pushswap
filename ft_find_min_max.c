/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_min_max.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:46:40 by graybaud          #+#    #+#             */
/*   Updated: 2016/02/16 11:33:50 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_find_min_max(t_env *env)
{
	t_nod	*save;
	t_nod	*stop;

	stop = LA_PRV;
	save = LA_NXT;
	MIN_A = LA_NXT;
	MAX_A = LA_NXT;
	env->flag |= F_MUTE;
	while (LA_NXT != stop)
	{
		MIN_A = (LA_NB1 < MIN_A->nb) ? LA_NXT : MIN_A;
		MAX_A = (LA_NB1 > MAX_A->nb) ? LA_NXT : MAX_A;
		ft_ra(env);
	}
	MIN_A = (LA_NB1 < MIN_A->nb) ? LA_NXT : MIN_A;
	MAX_A = (LA_NB1 > MAX_A->nb) ? LA_NXT : MAX_A;
	ft_move_to_nod(env, save);
	env->flag &= ~F_MUTE;
}
