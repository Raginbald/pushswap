/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 16:28:46 by graybaud          #+#    #+#             */
/*   Updated: 2015/11/23 14:26:26 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "main.h"

static void	ft_end(const char *str, int flag)
{
	ft_putstr_fd(str, flag + 1);
	exit(flag);
}

static void	ft_end_env(t_env *env, const char *str, int flag)
{
	free(env);
	env = NULL;
	ft_end(str, flag);
}

static void	ft_end_env_root(t_env *env, const char *str, int flag)
{
	if (env->a)
	{
		if (env->a->head)
			ft_free_list(env->a->head, &env->a->len);
		free(env->a);
		env->a = NULL;
	}
	if (env->b)
	{
		if (env->b->head)
			ft_free_list(env->b->head, &env->b->len);
		free(env->b);
		env->a = NULL;
	}
	ft_end_env(env, str, flag);
}

void		ft_exit(t_env *env, const char *str, int flag)
{
	if (!env)
		ft_end(str, flag);
	else if (!env->a && !env->b)
		ft_end_env(env, str, flag);
	else
		ft_end_env_root(env, str, flag);
}
