/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_b.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/05 11:38:42 by graybaud          #+#    #+#             */
/*   Updated: 2015/10/05 11:38:43 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "main.h"

int		ft_abs(int n)
{
	return (n * (n > 0) + ((n > 0) - 1) * n);
}

int		ft_isdigit(const int c)
{
	return ((c <= 57 && c >= 48));
}

int		ft_iswhitespace(const int c)
{
	return ((c == 32) || (c >= 9 && c <= 13));
}

int		ft_isdigit_str(const char *str)
{
	while (*str && (ft_iswhitespace(*str) || str[0] == '-' || str[0] == '+'))
		++str;
	while (*str)
	{
		if (!ft_isdigit(*str))
			return (0);
		else
			++str;
	}
	return (1);
}

int		ft_atoi(t_env *env, const char *str)
{
	int			neg;
	int			i;
	long		n;

	if (!str)
		return (0);
	i = 0;
	n = 0;
	neg = 1;
	while (ft_iswhitespace(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			neg = -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		n = n * 10 + (str[i] - '0');
		i++;
	}
	if (n < INT_MIN || n > INT_MAX)
		ft_exit(env, ERROR, 0);
	return ((int)n * neg);
}
